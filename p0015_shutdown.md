# Terminal: Desligando ou reiniciando o computador pelo terminal

Para desligar o computador através do terminal, digite:

```
$ shutdown now
```

- **$** indica que você deve usar o **usuário comum** para fazer essa operação.

- **sudo** serve para pedir permissões de administrador temporariamente.

- **shutdown** para **desligado** a máquina.

- **now** diz a máquina para **desligar imediatamente**.

Caso queira reiniciar a máquina, adicionar um argumento, assim:

```
$ shutdown -r now
```

- **r**, do inglês *reboot*, para finalizar os processos no sistema e reiniciar a máquina.

tags: tutorial, linux, shutdown, desligar, reiniciar
